# Setting up Vim like me
These instructions are written for Ubuntu 16.10 and for a single specific 
use-case (mine). That said they should be easily tweak-able for other Unix systems
and personal preferences.

## Installing Vim
```
sudo apt-get install vim-gtk3
```
If on Ubuntu 16.04
```
sudo add-apt-repository ppa:jonathonf/vim
```

## Installing Plugins and Configuring Vim (Required)
```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://gitlab.com/jhring/Johns-Vim-Setup
cp Johns-Vim-Setup/.vimrc ~/.vimrc
mkdir -p ~/.vim/after/syntax
cp John-Vim-Setup/python.vim ~/.vim/after/syntax
From Vim run :PluginInstall
cd ~/.vim/bundle/youcompleteme
sudo apt-get install cmake build-essential python-dev
./install.py --clang-completer
```
Note for latex compiler support you will need to run:
```
sudo apt-get install latexmk
```

## Font Setup (Recommended)
My vimrc is set up to use a powerline patched version of Adobe Source code
Pro if available.
```
# clone
git clone https://github.com/powerline/fonts.git
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts
```

## Acknowledgements
Thank you to all the Vim and vim-plugin developers who made this configuration
possible.
+ https://github.com/Valloric/YouCompleteMe
+ https://github.com/VundleVim/Vundle.vim
+ https://github.com/vim-syntastic/syntastic
+ https://github.com/SirVer/ultisnips
+ https://github.com/honza/vim-snippets
+ https://github.com/tpope/vim-sensible
+ https://github.com/vim/vim
+ https://github.com/vim-airline/vim-airline
+ https://github.com/powerline/fonts
+ https://github.com/tpope/vim-fugitive
+ https://github.com/lervag/vimtex
